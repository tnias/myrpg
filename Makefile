BLD=build
SRC=src
RES=res

BIN=$(BLD)/myrpg

.PHONY: all cmakee run val edit clean wc this mooo

all: cmake run

cmake:
	@mkdir -pv $(BLD)
	@cd $(BLD) && cmake ../$(SRC) && make VERBOSE=1
	@cp -rv $(RES) $(BLD)

run: $(BIN)
	@./$(BIN)

val: $(BIN)
	@valgrind \
		--leak-check=yes \
		--track-origins=yes \
		./$(BIN)

edit:
	@sh ./.vim.sh

clean:
	@rm -rfv $(BLD)

wc:
	@find ./$(SRC) -name "*.*" | xargs wc -l
	@cloc ./$(SRC)

this:
	@echo "make this, make that..."

mooo:
	@echo "moooo-superpowers enabled makefile"


