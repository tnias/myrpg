#include "world.h"

world_t *world_create(char *name)
{
	world_t *w = malloc(sizeof(world_t));

	w->locations = slst_create();

	w->name = strdup(name);
	w->desc = WORLD_DEFAULT_DESC;

	return w;
}

void world_load(world_t *world) 
{
	const char *filename = WORLD_DEFAULT_FILE;

	FILE *file;
	
	yaml_parser_t parser;
	yaml_event_t event;

	int done = 0;
	int count = 0;
	int error = 0;
	
	file = fopen(filename , "rb");

	if (!file) {
		log_err("failed to open file descriptor");
		return;
	}

	if (!yaml_parser_initialize(&parser)) {
		log_err("failed to initialize parser");
		return;
	}

	yaml_parser_set_input_file(&parser, file);

	while (!done) {
		if (!yaml_parser_parse(&parser, &event)) {
			error = 1;
			break;
		}

		done = (event.type == YAML_STREAM_END_EVENT);

		yaml_event_delete(&event);

		count++;
	}

	yaml_parser_delete(&parser);
	
	fclose(file);

	printf("%s (%i)\n", (error ? "ERROR" : "SUCCESS"), count);

	world_print(world);
}

void world_print(world_t *w)
{
	printf("world: %s (%s)\n", w->name, w->desc);

	for (size_t i = 0; i < w->locations->size; i++) {
		printf("%zu", i);
		location_print((location_t *) slst_get(w->locations, i));
	}
}

void world_free(world_t *w)
{
	if (w->locations) {
		slst_free(w->locations);
		free(w->locations);
	}

	if (w->name != NULL)
		free(w->name);
}

