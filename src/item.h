#ifndef ITEM_H
#define ITEM_H

#include "include.h"

#include "effect.h"

typedef enum {
	ITEM_UNKNOWN,
	ITEM_WEARABLE,
	ITEM_WEAPON,
	ITEM_CONSUMABLE,
} itemcat_t;

typedef struct {
	itemcat_t type;

	char* name;

	effect_t effect;	// effect (can be used by dmg calculations)

	double weight;
	double price;

	int equipable;		// can be put certain inventorys
				// needed to be checked by function responsible
				// for handeling inventorys and stuff
} item_t;

item_t *item_create(char *name, itemcat_t type);

void item_free(item_t *item);

void item_print(item_t *item);

#endif
