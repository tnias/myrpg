#ifndef WORLD_H
#define WORLD_H

#include <yaml.h>

#include "include.h"

#include "location.h"


#define WORLD_DEFAULT_FILE "res/world.yaml"

#define WORLD_DEFAULT_NAME "unnamed world"
#define WORLD_DEFAULT_DESC "- no description -"

typedef struct {
	char *name;
	char *desc;
	
	slst *locations;
} world_t;

world_t *world_create(char *name);

void world_load(world_t *world);

void world_print(world_t *w);

void world_free(world_t *w);

#endif
