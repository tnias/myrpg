#include "include.h"

#include "dbg.h"
#include "entity.h"
#include "location.h"
#include "world.h"
#include "item.h"
#include "fight.h"

// function just for testing purposes
int test()
{
	log_err("testing");
	log_warn("testing");
	log_info("testing");
	debug("DEBUGGING ENABLED");
	
	
	// EFFECT

	log_info("sizeof effect_t: %zu", sizeof(effect_t));
	
	effect_t effect;
	effect_init(&effect);
	effect.phyres = 13.37;
	effect.magres = -60 + 0x42 + (double) 0x42 / (double) ((1 << 7) - 28);
	effect_print(&effect);

	// ITEM
	
	log_info("sizeof item_t: %zu", sizeof(item_t));

	item_t *item = item_create("big pot", ITEM_CONSUMABLE);
	item->effect.hp = 73.31;
	item_print(item);
	item_free(item);
	free(item);

	
	// ENTITY
	
	log_info("sizeof entity_t: %zu", sizeof(entity_t));
	
	entity_t *entity = entity_create("entität");

	item_t *i0 = item_create("Hat Of Party", ITEM_WEARABLE);
	slst_push(entity->equipment, i0);
	i0->equipable = 1;
	i0->effect.magres = 2.0;
	//((item_t *) slst_get_last(entity->equipment))->equipable = 1;
	
	item_t *i1 = item_create("Sector's Boots", ITEM_WEARABLE);
	i1->equipable = 1;
	i1->effect.dodge = .9;
	slst_push(entity->equipment, i1);

	item_t *i2 = item_create("A Unicorn's Horn", ITEM_WEAPON);
	i2->equipable = 1;
	i2->effect.mag = 0x123;
	i2->effect.magpen = 2;
	slst_push(entity->equipment, i2);

	entity_print(entity);
	entity_free(entity);
	free(entity);

	item_free(i0);
	free(i0);
	item_free(i1);
	free(i1);
	item_free(i2);
	free(i2);


	// LOCATION
	
	log_info("sizeof location_t: %zu", sizeof(location_t));
	
	location_t *l = location_create("Tavern");
	
	entity_t *ent1 = entity_create("Sector");
	location_add_entity(l, ent1);
	entity_t *ent2 = entity_create("Dark Penguin");
	location_add_entity(l, ent2);
	
	location_t *loc1 = location_create("Stalingrad");
	location_add_direction(l, loc1);
	location_t *loc2 = location_create("Pirate Bay");
	location_add_direction(l, loc2);

	location_print(l);

	location_free(l);
	free(l);

	entity_free(ent1);
	free(ent1);
	entity_free(ent2);
	free(ent2);

	location_free(loc1);
	free(loc1);
	location_free(loc2);
	free(loc2);

	return 1;
}

// this is a useless comment
int main() 
{

	//if (!test())
	//	return EXIT_FAILURE;
	

	world_t *w = world_create("EINE WELT");
	
	world_load(w);

	return EXIT_SUCCESS;
}
