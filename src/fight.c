#include "fight.h"

// lets a human controlled Entity fight against a computer controlled one
// returns wether or not the human has won
// TODO: feature for human interaction is missing
int fight(entity_t *hmn, entity_t *cpu) 
{
	printf("TODO chance for hit or miss goes here!?\n");

	effect_t h = entity_get_damage(hmn);
	effect_t c = entity_get_damage(cpu);


	entity_apply_effect_damage(cpu, &h);
	entity_apply_effect_damage(hmn, &c);

	return 1;
}
