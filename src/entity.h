#ifndef ENTITY_H
#define ENTITY_H

#include "include.h"

#include "effect.h"
#include "item.h"

// TODO: check if default values are choosen wisely

#define ENTITY_DEF_HP  		100.0

#define ENTITY_DEF_REL		0.99

#define ENTITY_DEF_PHY		5.56
#define ENTITY_DEF_MAG		0.00
#define ENTITY_DEF_PHYRES 	0.91
#define ENTITY_DEF_MAGRES	0.92
#define ENTITY_DEF_PHYPEN	1.03
#define ENTITY_DEF_MAGPEN	1.04

#define ENTITY_DEF_HIT		0.93
#define ENTITY_DEF_DODGE	0.85
#define ENTITY_DEF_CRIT		0.42
#define ENTITY_DEF_CRIT_MUL	2.56

#define ENTITY_DEF_TRADE 	1.337

typedef struct entity_t {
	char *name;
	char *description;

	// stats
	effect_t stats;
	
	slst *inventory;
	slst *equipment;
} entity_t;

entity_t *entity_create(char *name);

effect_t entity_get_damage(entity_t *entity);

int entity_apply_effect(entity_t *entity, effect_t *effect);
int entity_apply_effect_stats(entity_t *entity, effect_t *effect);
int entity_apply_effect_damage(entity_t *entity, effect_t *effect);

int entity_isdead(entity_t *entity);

void entity_rename(entity_t *entity, char *name);

void entity_set_text(entity_t *entity, char *text);

void entity_free(entity_t *entity);

void entity_print(entity_t *entity);

#endif
