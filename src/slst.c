#include "slst.h"

slst *slst_create()
{
	slst *s = malloc(sizeof(slst));

	s->size = 0;
	s->first = NULL;
	s->last = NULL;

	return s;
}

void slst_free(slst *list)
{
	while(list->size)
		slst_pop(list);
}

int slst_isempty(slst *list) 
{
	return list->size == 0 ? 1 : 0;
}

void *slst_get_first(slst *list) 
{
	return list->first->ptr;
}

void *slst_get_last(slst* list)
{
	return list->last->ptr;
}

void *slst_get(slst *list, size_t index)
{
	if (index >= list->size)
		return NULL;

	return    (list->size >> 1) > index 
		? slst_get_front(list->first, index)
		: slst_get_back(list->last, list->size - (index + 1));
}

void *slst_get_front(slst_node *node, size_t index) 
{
	if (index == 0)
		return node->ptr;

	return slst_get_front(node->next, index-1);
}

void *slst_get_back(slst_node *node, size_t index)
{
	if (index == 0)
		return node->ptr;

	return slst_get_back(node->prev, index-1);
}

void slst_push(slst *list, void *ptr)
{
	slst_node *node = malloc(sizeof(slst_node));

	node->ptr = ptr;

	if (list->last == NULL) {
		list->first = node;
		list->last = node;
	} else {
		list->last->next = node;
		node->prev = list->last;
		list->last = node;
	}

	list->size++;
	
	return;
}

void *slst_pop(slst *list) 
{
	slst_node *node = list->last;

	return node ? slst_remove(list, node) : NULL;
}

void *slst_remove(slst *list, slst_node *node)
{
	void *ret = NULL;

	if (!(list->first && list->last) || !node)
		return NULL;

	if (node == list->first && node == list->last) {
		list->first = NULL;
		list->last = NULL;
	} else if (node == list->first) {
		list->first = node->next;
		list->first->prev = NULL;
	} else if (node == list->last) {
		list->last = node->prev;
		list->last->next = NULL;
	} else {
		slst_node *tmp_prev = node->prev;
		slst_node *tmp_next = node->next;
		tmp_prev->next = tmp_next;
		tmp_next->prev = tmp_prev;
	}

	list->size--;
	ret = node->ptr;
	free(node);

	return ret;
}
