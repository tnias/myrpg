#include "entity.h"

// creating an Entity
entity_t *entity_create(char *name) 
{
	entity_t *entity = malloc(sizeof(entity_t));

	entity->name = strdup(name);
	entity->description = NULL;

	effect_init(&entity->stats);
	
	// set default stats
	
	entity->stats.hp = ENTITY_DEF_HP;

	entity->stats.rel = ENTITY_DEF_REL;
	
	entity->stats.phy = ENTITY_DEF_PHY;
	entity->stats.mag = ENTITY_DEF_MAG;
	entity->stats.phyres = ENTITY_DEF_PHYRES;
	entity->stats.magres = ENTITY_DEF_MAGRES;
	entity->stats.phypen = ENTITY_DEF_PHYPEN;
	entity->stats.magres = ENTITY_DEF_MAGPEN;

	
	entity->stats.hit = ENTITY_DEF_HIT;
	entity->stats.dodge = ENTITY_DEF_DODGE;
	entity->stats.crit = ENTITY_DEF_CRIT;
	entity->stats.crit_mul = ENTITY_DEF_CRIT_MUL;
	
	entity->stats.trade = ENTITY_DEF_TRADE;

	// initialize both 'inventories'

	entity->inventory = slst_create();
	entity->equipment = slst_create();
	
	return entity;
}

effect_t entity_get_stats(entity_t *entity)
{
	effect_t stats;
	memcpy(&stats, &entity->stats, sizeof(effect_t));

	for (size_t i = 0; i < entity->equipment->size; i++) {
		switch(((item_t *) slst_get(entity->equipment, i))->type) {
		case ITEM_WEAPON:
		case ITEM_WEARABLE:
			stats = effect_add(
				stats, 
				((item_t *) slst_get(entity->equipment, i))->effect
				);
			break;		
		case ITEM_UNKNOWN: // fallthrough
		default:
			log_err("%i (itemcat_t) unknown", 
				((item_t *) slst_get(entity->equipment, i))->type);
		}
	}

	return stats;
}

effect_t entity_get_damage(entity_t *entity)
{
	entity->stats.hp++;
	entity->stats.hp--;

	printf("TODO: calculate damage of entity based on items carrying\n");
	
	effect_t dmg;
	effect_init(&dmg);
	
	dmg.phy = 5.0;
	dmg.phyres = 0;

	dmg.mag = 5.0;
	dmg.magres = 0;

	return dmg;
}

// apply damage to entity
int entity_apply_effect(entity_t *entity, effect_t *effect) 
{
	// TODO: only applys damage, other effects are needed
	entity->stats.hp += effect->hp;
	return entity_isdead(entity);
}

// apply stat changes to entity
int entity_apply_effect_stats(entity_t *entity, effect_t *effect) 
{
	entity->stats.hp += effect->hp;
	entity->stats.trade *= (1.0 - effect->trade);
	return entity_isdead(entity);
}

// apply damage to entity
int entity_apply_effect_damage(entity_t *entity, effect_t *effect) 
{
	// absolute dmg
	entity->stats.hp -= effect->hp;

	// physical damage
	// effect->phyres is physical penetration
	entity->stats.hp -= 
		effect->phy 
		* (1 - entity->stats.phyres) 
		* (1 + effect->phyres);

	// magical damage
	// effect->magres is magical penetration
	entity->stats.hp -= 
		effect->mag 
		* (1 - entity->stats.magres) 
		* (1 + effect->magres);

	return entity_isdead(entity);
}

int entity_isdead(entity_t *entity)
{
	return entity->stats.hp > 0;
}

// rename entity
void entity_rename(entity_t *entity, char *name) 
{
	if (entity->name != NULL)
		free(entity->name);

	entity->name = strdup(name);
}

// set text
void entity_set_description(entity_t *entity, char *text)
{
	if (entity->description != NULL)
		free(entity->description);
	
	entity->description = strdup(text);
}

// free entity_t from memory
void entity_free(entity_t *entity) 
{
	if (entity->name != NULL) {
		free(entity->name);
		entity->name = NULL;
	}

	slst_free(entity->inventory);
	free(entity->inventory);
	slst_free(entity->equipment);
	free(entity->equipment);
}

// print entity_tstats to stdout
void entity_print(entity_t *entity) 
{
	printf("entity: %s (@ %p)\n", 
		entity->name, 
		entity
		);
		
	printf("\tinventory: \n");
	for (size_t i = 0; i < entity->inventory->size; i++) {
		printf("\t\t%s (@ %p)\n", 
			((item_t *) slst_get(entity->inventory, i))->name,
			slst_get(entity->inventory, i)
			);

	}

	printf("\tequipment: \n");
	for (size_t i = 0; i < entity->equipment->size; i++) {
		printf("\t\t%s (@ %p)\n", 
			((item_t *) slst_get(entity->equipment, i))->name,
			slst_get(entity->equipment, i)
			);

	}
	
	// TODO: print stats
	/*
	printf("raw stats:\n");
	effect_print(&entity->stats);
	
	printf("actual stats:\n");
	effect_t e = entity_get_stats(entity);
	effect_print(&e);
	*/
}
