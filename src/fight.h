#ifndef FIGHT_H
#define FIGHT_H

#include "include.h"

#include "entity.h"

int fight(entity_t *human, entity_t *cpu);

#endif
