/*
 * slst (simpleList) is a double linked list implementation
 */

#ifndef SLST_H
#define SLST_H

#include <stdlib.h>

typedef struct slst_node {
	struct slst_node *next;
	struct slst_node *prev;
	void *ptr;
} slst_node;

typedef struct slst {
	size_t size;
	slst_node *first;
	slst_node *last;
} slst;

// create slst struct that is a container for the nodes
slst *slst_create();
// and remove all its nodes
void slst_free(slst *list);

// push a new node to the end
void slst_push(slst *l, void *ptr);

// get the last node and remove it
void *slst_pop(slst *list);

// get ptr of first and last element
void *slst_get_first(slst* list);
void *slst_get_last(slst* list);

// get stuff out of the list by index
void *slst_get(slst *list, size_t index);
void *slst_get_front(slst_node *node, size_t index);
void *slst_get_back(slst_node *node, size_t index);

// remove a node from the list
void *slst_remove(slst *list, slst_node *node);

#endif
