#include "effect.h"

// initialization sets all values to 0
void effect_init(effect_t *effect) 
{
	effect->hp  = 0.0;
	effect->rel = 1.0;

	effect->phy    = 0.0;
	effect->mag    = 0.0;
	effect->phyres = 1.0;
	effect->magres = 1.0;
	effect->phypen = 1.0;
	effect->magpen = 1.0;

	effect->hit      = 1.0;
	effect->dodge    = 1.0;
	effect->crit     = 1.0;
	effect->crit_mul = 1.0;

	effect->trade = 1.0;
}

// add effects (especialy for adding stats and stuff)
effect_t effect_add(effect_t a, effect_t b)
{
	// TODO verify calculations:
	// 	ARE THEY SANE????
	a.hp        += b.hp;
	a.rel       *= b.rel;
	
	a.phy       += b.phy;
	a.mag       += b.mag;
	a.phyres    *= b.phyres;
	a.magres    *= b.magres;
	a.phypen    *= b.phypen;
	a.magpen    *= b.magpen;

	a.hit       *= b.hit;
	a.dodge     *= b.dodge;
	a.crit      *= b.crit;
	a.crit_mul  *= b.crit_mul;

	return a;
}

void effect_print(effect_t *effect) 
{
	printf("effect:\n\t"
		"hp:       %7.2f\n\t"
		"rel:      %7.2f\n\t"
		"phy:      %7.2f\n\t"
		"mag:      %7.2f\n\t"
		"phyres:   %7.2f\n\t"
		"magres:   %7.2f\n\t"
		"phypen:   %7.2f\n\t"
		"magpen:   %7.2f\n\t"
		"hit:      %7.2f\n\t"
		"dodge:    %7.2f\n\t"
		"crit:     %7.2f\n\t"
		"crit_mul: %7.2f\n\t"
		"trade:    %7.2f\n\t"
		"\n",
		effect->hp, 
		effect->rel,
		effect->phy,
		effect->mag,
		effect->phyres,
		effect->magres,
		effect->phypen,
		effect->magpen,
		effect->hit,
		effect->dodge,
		effect->crit,
		effect->crit_mul,
		effect->trade
		);
}
