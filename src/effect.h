#ifndef EFFECT_H
#define EFFECT_H

#include "include.h"

typedef struct {
	double hp;		// absolute health manipulation
	double rel;		// relative health manipulation

	double phy;		// physical 'damage'
	double mag;		// magical 'damage' 
	double phyres;		// physical 'resistance'
	double magres;		// mygical 'resistance'
	double phypen;		// physical penetration
	double magpen;		// magical penetration

	double hit;		// successful hit
	double dodge; 		// chance to get hit
	double crit;		// land a critical hit
	double crit_mul;	// critical damage multiplicator 
	
	double trade;		// priceing factor for buying and selling
} effect_t;

void effect_init(effect_t *effect);

effect_t effect_add(effect_t a, effect_t b);

void effect_print(effect_t *effect);

#endif
