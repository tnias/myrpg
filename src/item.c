#include "item.h"

item_t *item_create(char *name, itemcat_t type)
{
	item_t *item = malloc(sizeof(item_t));
	
	item->name = strdup(name);
	
	item->type = type;
	
	effect_init(&item->effect);

	item->weight = 0;
	item->price = 0;

	item->equipable = 0;

	return item;
}

void item_rename(item_t *item, char *name) 
{
	if (item->name != NULL) {
		free(item->name);
	}

	item->name = strdup(name);
}

void item_print(item_t *item)
{
	printf(
		"Item name: %s (type: %i) @ %p\n",
		item->name,
		item->type,
		item
		);

	// uncomment for hight detail
	//effect_print(&item->effect);

}

void item_free(item_t *item) 
{
	if (item->name != NULL) {
		free(item->name);
		item->name = NULL;
	}
}
