#ifndef LOCATION_H
#define LOCATION_H

#include "include.h"

#include "slst.h"
#include "entity.h"

typedef struct location_t{
	slst *dirs;
	slst *entities;

	char *name;
	char *description;
} location_t;

location_t *location_create(char *);

void location_print(location_t *);

void location_free(location_t *);

void location_add_entity(location_t *, entity_t *);
void location_add_direction(location_t *, location_t *);

#endif
