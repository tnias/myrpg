#include "location.h"

// create a location
location_t *location_create(char *name) 
{
	location_t *location = malloc(sizeof(location_t));

	location->name  = strdup(name);
	location->description = NULL;

	location->dirs = slst_create();
	location->entities = slst_create();

	return location;
}

// prints
void location_print(location_t *location)
{
	printf("location: %s (@ %p)\n", location->name, location);
	
	printf("\tdirections:\n");
	for (size_t i = 0; i < location->dirs->size; i++) {
		printf("\t\t%zu. %s (@ %p)\n", 
			i+1, 
			((location_t *) slst_get(location->dirs, i))->name,
			(location_t *) slst_get(location->dirs, i)
			);
	}

	printf("\tentities:\n");
	for (size_t i = 0; i < location->entities->size; i++) {
		printf("\t\t%zu. %s (@ %p)\n", 
			i+1,
			((entity_t *) slst_get(location->entities, i))->name,
			(entity_t *) slst_get(location->entities, i)
			);
	}
}

// free all ressources allocated by p
void location_free(location_t *location)
{
	if (location->name != NULL) {
		free(location->name);
		location->name = NULL;
	}

	slst_free(location->dirs);
	free(location->dirs);

	slst_free(location->entities);
	free(location->entities);
}

// 
void location_add_entity(location_t *location, entity_t *entity)
{
	slst_push(location->entities, entity);
}

void location_add_direction(location_t *location, location_t *direction)
{
	slst_push(location->dirs, direction);
}
