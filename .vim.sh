#/bin/bash

src=$(mktemp /tmp/list.XXXXXX) || exit 1
find ./src -name "*.c" | sed -e "s/ /\n/g" > $src

echo "opening files:"

PARTA=""
PARTB=""

for SOURCE in $(cat $src)
do
	HEADER=$(echo $SOURCE | sed -e "s/\.c/\.h/g")
	test -f $HEADER || HEADER=""

	PARTA+="$SOURCE "
	PARTB+="vs $HEADER | tabn | "


	printf "$SOURCE \t $HEADER\n"

done

vim -p $PARTA -c "tabm 0 | $PARTB"

